-- local tracking_unit = nil

script.on_load(function()
  -- if true then return end
  -- tracking_unit = global.tracking_unit
  -- tracking_unit = nil
end)

local function float_to_string(num)
  return string.format("%.3f", num)
end

local function position_to_string(position)
  return "{" .. float_to_string(position['x']) .. ", " .. float_to_string(position['y']) .. "}"
end

local function floor(float)
  local int, _ = math.modf(float)
  return int
end

local function ceil(float)
  local int, frac = math.modf(float)
  if frac > 0 then
    return int + 1
  end
  return int
end

local function make_id(unit_number, position)
  if unit_number then return '' .. unit_number end
  return '' .. floor(position['x']) .. 'x' .. floor(position['y'])
end

-- in tiles
local half_screen_horiz = 128
local half_screen_vert = 64
local sector_size_horiz = 128
local sector_size_vert = 128
local sectors_horiz = ceil(half_screen_horiz * 2 / sector_size_horiz)
local sectors_vert = ceil(half_screen_vert * 2 / sector_size_vert)

local first_run = true

function get_player_next_sector(player)
  if first_run then
    global.player_sectors = {}
    first_run = false
  end
  global.player_sectors = global.player_sectors or {}
  local sector = global.player_sectors[player.index]
  if sector == nil then
    sector = {x=0, y=0}
    global.player_sectors[player.index] = sector
  end
  local next_sector = {x=sector.x, y=sector.y}
  if next_sector.x == sectors_horiz - 1 then
    next_sector.x = 0
    next_sector.y = next_sector.y + 1
  else
    next_sector.x = next_sector.x + 1
  end
  if next_sector.y == sectors_vert then
    next_sector.x = 0
    next_sector.y = 0
  end
  global.player_sectors[player.index] = next_sector
  return sector
end

function make_sector_id(sector)
  return '' .. sector.x .. 'x' .. sector.y
end

local moving_entities = { "car", "unit", "locomotive", "car", "spider-vehicle", "turret", "combat-robot", "fluid-turret", "logistic-robot", 
  "electric-turret", "construction-robot", "ammo-turret", "artillery-turret" }

function update_player(player)

  if true then return end

  local res = {}

  -- local pos = player.position
  -- for key, entity in pairs(game.get_surface('nauvis').find_enemy_units(game.get_player(1).position, 80)) do
  --   table.insert(res, { id=entity.unit_number, position={ x=(entity.position['x'] - pos['x']) / 8, y=(pos['y'] - entity.position['y']) / 8 }, orientation=entity.orientation })
  -- end

  local res_entities = {}
  local pos_x = player.position['x']
  local pos_y = player.position['y']

  local sector = get_player_next_sector(player)
  local screen_left = pos_x - half_screen_horiz
  local screen_top = pos_y - half_screen_vert
  local sector_left = screen_left + (sector.x * sector_size_horiz)
  local sector_top = screen_top + (sector.y * sector_size_vert)
  
  local left_top = {sector_left, sector_top}
  local right_bottom = {sector_left + sector_size_horiz, sector_top + sector_size_vert}
  local sector_rect = {left_top, right_bottom}
  
  res.sector = make_sector_id(sector)
  res.player = {pos=player.position}
  res.entities = res_entities

  -- Moving entities
  -- 
  local entities = game.get_surface('nauvis').find_entities_filtered{ area = sector_rect, type = moving_entities }
  for key, entity in pairs(entities) do
    table.insert(res_entities, { id=make_id(entity.unit_number, entity.position), name=entity.name, position=entity.position, orientation=entity.orientation })
  end

  -- Units only, on the screen, in sectors
  -- 
  -- for key, entity in pairs(game.get_surface('nauvis').find_units({ area = sector_rect, force = "player", condition = "all" })) do
  --   table.insert(entities, { id=make_id(entity.unit_number, entity.position), name=entity.name, position=entity.position, orientation=entity.orientation })
  -- end

  -- Entities on the screen, in sectors
  -- 
  -- for key, entity in pairs(game.get_surface('nauvis').find_entities(sector_rect)) do
  --   table.insert(entities, { id=make_id(entity.unit_number, entity.position), name=entity.name, position=entity.position, orientation=entity.orientation })
  -- end

  -- Entities in radius
  -- 
  -- for key, entity in pairs(game.get_surface('nauvis').find_entities_filtered{position=game.get_player(1).position, radius=80}) do
  --   table.insert(entities, { id=make_id(entity.unit_number, entity.position), name=entity.name, position=entity.position, orientation=entity.orientation })
  -- end

  -- Enemy units
  -- 
  -- for key, entity in pairs(game.get_surface('nauvis').find_enemy_units(game.get_player(1).position, 80)) do
  --   table.insert(entities, { id=entity.unit_number, name=entity.name, position=entity.position, orientation=entity.orientation })
  -- end

  game.write_file('updates', game.table_to_json(res) .. '\n', true, player.index)
end

script.on_event(defines.events.on_tick,
  function(event)
    -- local player = game.get_player(1)
    -- player.print(player.position)
    
    -- if event.tick % 5 ~= 0 then return end
    -- game.write_file('file1', '' .. event.tick .. '\n', true, 1)

    -- local player = game.get_player(1)

    -- if tracking_unit and tracking_unit.valid then
    --   player.print("position: " .. position_to_string(tracking_unit.position) .. ", orientation: " .. float_to_string(tracking_unit.orientation))
    -- end

    for _, player in pairs(game.players) do
      update_player(player)
    end
  end
)

script.on_event(defines.events.on_player_changed_position,
  function(event)
    if true then return end

    local player = game.get_player(event.player_index)
    local entities = game.get_surface('nauvis').find_entities_filtered{position=game.get_player(1).position, radius=80}
    local out = {}
    for key, entity in pairs(entities) do
      table.insert(out, { name = entity.name, type = entity.type })
    end
    game.write_file('entities.json', game.table_to_json(out))

    local player = game.get_player(event.player_index) -- get the player that moved
    -- local x = player.position['x']
    -- local y = player.position['y']

    for key, entity in pairs(game.get_surface("nauvis").find_entities_filtered{radius = 2, position = player.position, type = "character", invert = true}) do
      player.print("" .. key .. ": " .. entity.name)
    end

    -- if they're wearing our armor
    -- if player.character and player.get_inventory(defines.inventory.character_armor).get_item_count("fire-armor") >= 1 then
    --    -- create the fire where they're standing
    --    player.surface.create_entity{name="fire-flame", position=player.position, force="neutral"} 
    -- end
    -- player.surface.create_entity{name="fire-flame", position=player.position, force="neutral"} 
  end
)

script.on_event(defines.events.on_selected_entity_changed,
  function(event)
    if true then return end
    -- if true then return end
    local player = game.get_player(event.player_index)
    -- player.print("on_selected_entity_changed to " .. (player.selected and player.selected.unit_number or "nothing"))
    if player.selected and player.selected.type == "unit" then
      tracking_unit = player.selected
      global.tracking_unit = player.selected
      player.print("tracking unit " .. player.selected.unit_number)
    end
    -- player.print("walking: " .. tostring(player.walking_state.walking) .. ", direction: " .. player.walking_state.direction)
    -- player.print(player.name)
    -- player.print(shooting_state)
  end
)

script.on_event(defines.events.on_console_command,
  function(event)
    local player = game.get_player(event.player_index) -- get the player that entered command
    -- player.print("command entered: " .. event.command .. ' ' .. event.parameters)
    if event.command ~= 'capture' then
      if true then return end
    end
    player.print('capturing!')

    local entities = game.get_surface('nauvis').find_entities_filtered{ position = player.position, radius = 80, type = moving_entities }
    for key, entity in pairs(entities) do
      -- if entity.name == 'small-biter' then
        -- rendering.draw_rectangle{surface = player.surface, filled=true, left_top=entity, left_top_offset={-.25,-.5}, right_bottom=entity, right_bottom_offset={.25,0}, color={1,1,1}}
        rendering.draw_rectangle{surface = player.surface, filled=true, left_top=entity, left_top_offset={0,-.5}, right_bottom=entity, right_bottom_offset={1,.5}, color={1,1,1}}
        rendering.draw_rectangle{surface = player.surface, filled=true, left_top=entity, left_top_offset={.1,-.4}, right_bottom=entity, right_bottom_offset={.2,-.3}, color={0,0,0}}
      -- end
    end

    if true then return end

    -- player.print(game.surfaces["nauvis"].name)
    -- player.print(type(game.surfaces["nauvis"].find_entities({{-10, -10}, {10, 10}})))
    for key, entity in pairs(game.get_surface("nauvis").find_entities({{-10, -10}, {10, 10}})) do
      player.print("" .. key .. ": " .. entity.name)
    end
  end
)
